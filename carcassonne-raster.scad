size = 4;

bridgeheight = 1;
bridgewidth = 0.6;
tileside = 46;
baseheight = 1;
bridgebasewidth = 1;
clipradius = 3;
clipradiusspacing = 0.1;
clipheightspacing = 0.2;
clipoffset = 2.72;
clipsideset = 1;
clipinset = 1;

outerlength = tileside+2*bridgewidth;
completelength = outerlength * size;

module field() {
    difference() {
        cube([
            outerlength,
            outerlength,
            baseheight + bridgeheight
        ]);
        translate([bridgewidth, bridgewidth, -1]) {
            cube([
                tileside,
                tileside,
                bridgeheight + baseheight + 2
            ]);
        }
    }

    cutoutlength = outerlength * 1.2;
    difference() {
    cube([outerlength, outerlength, baseheight]);
    translate([outerlength/2, outerlength/2, baseheight/2]) {
        rotate([0,0,45]) {
            cube(
                    [
                        cutoutlength * 0.88,
                        cutoutlength * 0.88,
                        baseheight + 2
                    ],
                    center = true
                );
            }
        }
    }
    
    difference() {
        cube([outerlength, outerlength, baseheight]);
        translate(
            [
                bridgewidth + bridgebasewidth,
                bridgewidth + bridgebasewidth,
                -1
            ]
        ) {
            cube(
                [
                    tileside - 2 * bridgebasewidth,
                    tileside - 2 * bridgebasewidth,
                    baseheight + 2
                ]
            );
        }
    }
}

module fields(count) {
    for (i = [0:count-1], j = [0:count-1]) {
        translate([i*outerlength, j*outerlength, 0]) field();
    }
}

module nibbles_raw_side(count, height, radius) {
    for (i = [ 1:count-1]) {
        translate([i * outerlength+clipsideset, 0, 0]) {
            cylinder(
                h=height,
                r=radius
            );
        }
    }
}

module nibbles_side_outtakes(count) {
    translate([clipoffset, clipinset, -0.1]) {
        nibbles_raw_side(
            size,
            baseheight+clipheightspacing,
            clipradius+clipradiusspacing
        );
    }
}

module nibbles_side(count) {
    translate([-clipoffset, -clipinset, 0]) {
        nibbles_raw_side(
            size,
            baseheight,
            clipradius
        );
    }
}

module nibbles_outtakes(count) {
    nibbles_side_outtakes(size);
    translate([completelength, 0, 0]) {
        rotate([0,0,90]) nibbles_side_outtakes(size);
    }
    translate([completelength, completelength, 0]) {
        rotate([0,0,180]) nibbles_side_outtakes(size);
    }
    translate([0, completelength, 0]) {
        rotate([0,0,270]) nibbles_side_outtakes(size);
    }
}

module nibbles(count) {
    nibbles_side(size);
    translate([completelength, 0, 0]) {
        rotate([0,0,90]) nibbles_side(size);
    }
    translate([completelength, completelength, 0]) {
        rotate([0,0,180]) nibbles_side(size);
    }
    translate([0, completelength, 0]) {
        rotate([0,0,270]) nibbles_side(size);
    }
}

difference() {
    fields(size);
    nibbles_outtakes(size);
}

nibbles(size);
